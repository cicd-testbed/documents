What are containers?
    A container is a standalone executable package that includes an application and its dependencies. It can be easily moved and deployed across multiple computers and operating systems. Containers have everything the application needs to run, and they can function in the same way regardless of the underlying infrastructure. 


How to build a container
    To build a container, you need to first choose a service like a docker, Buildah, or podman and install the service. Then, for this example, we will use docker, so the next step is to create a docker file that contains the instructions for building the image. This includes what operating system/ base image, what software or files to include, and what commands to run during the image creation. In the case of docker, you would use the docker build command to build the image, then from there, you can run it using docker run and wala you have built a container. 


Why use containers  
    Containers offer many advantages to regular or VM-based deployments. The main advantages are consistency, portability, scalability, and isolation. Consistency in the case of containers means that each container contains all the dependencies it needs to run consistently across multiple environments mitigating the problems caused by different OS versions and other software. The container provides the application with the same environment on any system. Portability means that a container can run on any system that supports consideration regardless of the underlying infrastructure or operating systems, making it easy to move the application between different environments, cloud included. Scalability containers are designed for easy scalability as you can create multiple instances of the same container on the same host without significant performance change. Lastly, Isolation containers are highly separated as each application and dependencies are separate from both other containers and the host, so this significantly reduces conflicts between applications and increases security. 





